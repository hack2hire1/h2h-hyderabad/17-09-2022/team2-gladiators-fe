import { Button } from 'react-bootstrap';
import { Routes, Route } from 'react-router-dom';
import Manager from './Manager';
import './App.scss';

function App() {
  return (
    <div className='cont my-4'>
      <Routes>
        <Route exact path='/manager/:id' element={<Manager />} />
        <Route exact path='/employee' />
      </Routes>
    </div>
  );
}

export default App;
