import React, { useState, useEffect } from 'react';
import MapSection from './Map';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { employees } from './MockData';
const location = {
  address: '1600 Amphitheatre Parkway, Mountain View, california.',
  lat: 37.42216,
  lng: -122.08427,
};
const Manager = () => {
  const { id } = useParams();
  console.log(id);
  // const [employees, setEmployees] = useState([]);
  // useEffect(() => {
  //   axios
  //     .get('')
  //     .then((result) => {
  //       setEmployees(result.data);
  //     })
  //     .catch((err) => console.log('error'));
  // }, []);
  return (
    <div>
      <h2 className='managers my-5'>Manager's View</h2>
      <section>
        <table className='mgr-table'>
          <tr className='header-class'>
            <th>Emp Id</th>
            <th>Name</th>
            <th>Availability</th>
            <th>Comments</th>
            <th>Location</th>
          </tr>
          {employees.map((el) => (
            <tr key={el.employeeId} className='header-class'>
              <td>{el.employeeId}</td>
              <td>{el.employeeName}</td>
              <td>{el.availability ? 'Yes' : 'No'}</td>
              <td>{el.comments}</td>
              <td>{el.location}</td>
            </tr>
          ))}
        </table>
      </section>
      {/* <MapSection location={location} zoomLevel={17} /> */}
    </div>
  );
};

export default Manager;
