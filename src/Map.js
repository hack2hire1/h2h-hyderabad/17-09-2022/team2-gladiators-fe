import React from 'react';
import GoogleMapReact from 'google-map-react';
import LocationPin from './LocationPin';
import './Map.css';

const Map = ({ location, zoomLevel }) => (
  <div className='map'>
    <h2 className='map-h2'>Come Visit Us At Our Campus</h2>

    <div className='google-map'>
      <GoogleMapReact
        // bootstrapURLKeys={{ key: 'AIzaSyAuKOJ_9YWLn0MY_09ssN-1OLFAbKofurM' }}
        bootstrapURLKeys={{ key: 'AIzaSyDyf1khSQj1cAG7PEymYj38CcRjMCI78Yo' }}
        defaultCenter={location}
        defaultZoom={zoomLevel}
      >
        <LocationPin
          lat={location.lat}
          lng={location.lng}
          text={location.address}
        />
      </GoogleMapReact>
    </div>
  </div>
);

export default Map;
