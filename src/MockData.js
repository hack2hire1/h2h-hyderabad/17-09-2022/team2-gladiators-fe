export const employees = [
  {
    employeeId: '201',
    employeeName: 'Harsha',
    availability: true,
    comments: 'Punched-In',
    location: 'Hyderabad',
  },
  {
    employeeId: '301',
    employeeName: 'Nadeem',
    availability: true,
    comments: 'Punched-In',
    location: 'Hyderabad',
  },
  {
    employeeId: '401',
    employeeName: 'Harsha',
    availability: true,
    comments: 'Punched-In',
    location: 'Hyderabad',
  },
];
